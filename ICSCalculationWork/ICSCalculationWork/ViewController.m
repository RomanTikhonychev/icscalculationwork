//
//  ViewController.m
//  ICSCalculationWork
//
//  Created by Рома on 5/26/16.
//  Copyright © 2016 RomanTikhonychev. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <NSTextFieldDelegate, NSControlTextEditingDelegate>

@property (weak) IBOutlet NSTextField *createInfoLabel;
@property (weak) IBOutlet NSTextField *createLoginTextField;
@property (weak) IBOutlet NSTextField *createPasswordTextField;
@property (weak) IBOutlet NSButton *createButton;
@property (weak) IBOutlet NSTextField *createInfoTextView;


@property (weak) IBOutlet NSTextField *loginInfoLabel;
@property (weak) IBOutlet NSTextField *loginLoginTextField;
@property (weak) IBOutlet NSTextField *loginPasswordTextField;
@property (weak) IBOutlet NSButton *loginButton;
@property (weak) IBOutlet NSTextField *loginInfoTextView;


@property (nonatomic, strong) NSMutableArray *timeIntervals;
@property (nonatomic, strong) NSMutableArray *loginTimeIntervals;
@property BOOL learnMode;
@property (nonatomic, strong) NSString *learnLogin;
@property (nonatomic, strong) NSString *learnPassword;

@property (nonatomic, strong) NSMutableArray *plistArray;
@property (nonatomic, strong) NSString *aFilePath;

@property (nonatomic, assign) float persise;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.timeIntervals = [NSMutableArray new];
    self.loginTimeIntervals = [NSMutableArray new];
    self.createPasswordTextField.tag = 100;
    self.loginPasswordTextField.tag = 200;
    self.persise = 0.75;
    
    
    NSString *aDocumentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    self.aFilePath = [NSString stringWithFormat:@"%@/ICSAuthentificateData.plist", aDocumentsDirectory];
    self.plistArray = [[NSMutableArray alloc] initWithContentsOfFile:self.aFilePath];
    
    if (!self.plistArray) {
        self.plistArray = [NSMutableArray new];
    }
}


#pragma mark - Calculation Logic

- (IBAction)learnModeDidPushed:(id)sender {
    
    self.learnMode = !self.learnMode;
    [self.createInfoLabel setStringValue:[[NSString alloc] initWithFormat:@"Learn Mode: %i", (int)self.learnMode]];
    
    if (self.learnMode) {
        self.learnLogin = [self.createLoginTextField stringValue];
        self.learnPassword = [self.createPasswordTextField stringValue];
        [self.timeIntervals removeAllObjects];
        [self.createPasswordTextField setStringValue:@""];
    } else {
        self.learnLogin = nil;
        self.learnPassword = nil;
    }
}


- (void)learn
{
    int numberOfIntervals = (int)self.timeIntervals.count - 1;
    float *intervals = malloc (sizeof (float) * numberOfIntervals);
    float *intervalsCP = malloc (sizeof (float) * numberOfIntervals);
    for (int index = 0; index < numberOfIntervals; index++) {
        NSDate *firstDate = self.timeIntervals[index];
        NSDate *secondDate = self.timeIntervals[index + 1];
        NSTimeInterval executionTime = [secondDate timeIntervalSinceDate:firstDate];
        intervals[index] = (float)executionTime;
        intervalsCP[index] = intervals[index];
//         NSLog(@"time: %f y %f yc %f", (float)executionTime, intervals[index], intervalsCP[index]);
    }
    
    
    //Mi Si^2 Si
    float *Mi = malloc (sizeof (float) * numberOfIntervals);
    float *S2i = malloc (sizeof (float) * numberOfIntervals);
    float *Si = malloc (sizeof (float) * numberOfIntervals);
    float *Tp = malloc (sizeof (float) * numberOfIntervals);
    for (int i = 0; i < numberOfIntervals; i++) {
        
        //у '= у ^ {yi}
       float *intervalY = malloc (sizeof (float) * numberOfIntervals - 1);
       for (int n = 0; n < numberOfIntervals - 1; n++) {
            if (n < i) {
                intervalY[n] = intervals[n];
            } else {
                intervalY[n] = intervals[n + 1];
            }
        }
        
        //calculate Mi
        float sumY = 0;
        for (int k = 0; k < numberOfIntervals - 1; k++) {
            sumY += intervalY[k];
        }
        Mi[i] = sumY/(numberOfIntervals - 1);
        
        //calculate S^2i
        sumY = 0;
        for (int k = 0; k < numberOfIntervals - 1; k++) {
            sumY += (intervalY[k] - Mi[i])*(intervalY[k] - Mi[i]);
        }
        S2i[i] = sumY/(numberOfIntervals - 1 - 1);
        
        Si[i] = sqrtf(S2i[i]);
        
        Tp[i] = (intervals[i] - Mi[i])/Si[i];
        if (Tp[i] < 0) Tp[i] = (-Tp[i]);
        
        float buttom = Mi[i] - 3*Si[i];
        float top    = Mi[i] + 3*Si[i];
        if (buttom > intervalsCP[i] ||  top < intervalsCP[i]) intervalsCP[i] = 0;
//        NSLog(@"Si %f y %f yc %f", Si[i], intervals[i], intervalsCP[i]);
    }

    //fix mistakes
    for (int i = 0; i < numberOfIntervals; i++) {
        
        //у '= у ^ {yi}
        float *intervalY = malloc (sizeof (float) * numberOfIntervals - 1);
        for (int n = 0; n < numberOfIntervals - 1; n++) {
            if (n < i) {
                intervalY[n] = intervalsCP[n];
            } else {
                intervalY[n] = intervalsCP[n + 1];
            }
        }
        
        //calculate Mi
        float sumY = 0;
        for (int k = 0; k < numberOfIntervals - 1; k++) {
            sumY += intervalY[k];
        }
        Mi[i] = sumY/(numberOfIntervals - 1);
        
        //calculate S^2i
        sumY = 0;
        for (int k = 0; k < numberOfIntervals - 1; k++) {
            sumY += (intervalY[k] - Mi[i])*(intervalY[k] - Mi[i]);
        }
        S2i[i] = sumY/(numberOfIntervals - 1 - 1);
        
        Si[i] = sqrtf(S2i[i]);
    }
    
    NSString *previousString = @"";
    NSString *intervalString = @"";
    for (NSMutableDictionary *userInfo in self.plistArray) {
        if ([userInfo[@"login"] isEqualToString:self.learnLogin]) {
            
            previousString = userInfo[@"Si"];
            if (!previousString) {
                previousString = @"";
            }
            
            intervalString = userInfo[@"Inter"];
            if (!intervalString) {
                intervalString = @"";
            }
            
            NSArray *interComponents = [intervalString componentsSeparatedByString:@"/"];
            for (int g = 0; g < interComponents.count; g++) {
                intervals[g] = (intervals[g] + [interComponents[g] floatValue])/2.0;
            }
            
            NSString *interResultString = @"";
            for (int b = 0; b < numberOfIntervals; b++) {
                NSString *append = [[NSString alloc]initWithFormat:@"%f/", intervals[b]];
                interResultString = [interResultString stringByAppendingString:append];
            }
            [userInfo setValue:interResultString forKey:@"Inter"];
            
            
            NSArray *components = [previousString componentsSeparatedByString:@"/"];
            for (int g = 0; g < components.count; g++) {
                Si[g] = (Si[g] + [components[g] floatValue])/2.0;
            }
            NSString *resultString = @"";
            for (int b = 0; b < numberOfIntervals; b++) {
                NSString *append = [[NSString alloc]initWithFormat:@"%f/", Si[b]];
                resultString = [resultString stringByAppendingString:append];
            }
            NSLog(@"%@\n",resultString);
            [userInfo setValue:resultString forKey:@"Si"];
            [self.plistArray writeToFile:self.aFilePath atomically:NO];
        }
    }
}

#pragma mark - NSTextFieldDelegate

- (void)controlTextDidChange:(NSNotification *)notification {

    NSTextField *textField = [notification object];
    
    if (textField.tag == 100) {
        
        if (self.learnMode) {
            
            NSDate *time = [NSDate date];
            [self.timeIntervals addObject:time];
        }
    }
    
    if (textField.tag == 200) {
        
        NSDate *time = [NSDate date];
        [self.loginTimeIntervals addObject:time];
        
    }
}

- (IBAction)createPasTexFieldEnterPresed:(NSTextField *)sender {
    
    NSLog(@"enter pressed");
    NSString *checkPass = [sender stringValue];
    
    if (self.learnMode) {
        
        if ([checkPass isEqualToString:self.learnPassword]) {
        
            [self learn];
            [self.timeIntervals removeAllObjects];
            [self.createInfoLabel setStringValue:@"Learn done!"];
        } else {
            [self.createInfoLabel setStringValue:@"Learn wrong pass"];
        }
        
        [self.createPasswordTextField setStringValue:@""];
        
    }
    
}



#pragma mark - Create User

- (IBAction)createButtonDidPushed:(id)sender {
    [self createUser];
}

- (void)createUser {
    
    //check if exist
    NSString *currentLogin = [self.createLoginTextField stringValue];
    NSString *currentPassword = [self.createPasswordTextField stringValue];
    BOOL userAlreadyExist = NO;
    
    for (NSDictionary *userInfo in self.plistArray) {
        if ([userInfo[@"login"] isEqualToString:currentLogin]) {
            userAlreadyExist = YES;
        }
    }
    
    if (!userAlreadyExist) {
        
        NSDictionary *newUserInfo = @{@"login"    : currentLogin,
                                      @"password" : currentPassword,
                                      @"Si"       : @"",
                                      @"Inter"    : @""};
        
        [self.plistArray addObject:newUserInfo];
        [self.plistArray writeToFile:self.aFilePath atomically:NO];
        
        [self.createInfoLabel setStringValue: [[NSString alloc] initWithFormat:@"User %@ created",currentLogin]];
    } else {
        [self.createInfoLabel setStringValue:@"User already exist"];
    }
    
    NSString *output = @"";
    
    for (NSDictionary *userInfo in self.plistArray) {
        output = [output stringByAppendingString:@"L: "];
        output = [output stringByAppendingString:userInfo[@"login"]];
        output = [output stringByAppendingString:@" P: "];
        output = [output stringByAppendingString:userInfo[@"password"]];
        output = [output stringByAppendingString:@"\n"];
    }
    
    [self.createInfoTextView setStringValue:output];
}


#pragma mark - Authentificate User

- (IBAction)loginButtonDidPushed:(id)sender {
    
    NSString *loginLogin = [self.loginLoginTextField stringValue];
    NSString *loginPassword = [self.loginPasswordTextField stringValue];
    
    for (NSDictionary *userInfo in self.plistArray) {
        if ([userInfo[@"login"] isEqualToString:loginLogin]) {
            
            if ([userInfo[@"password"] isEqualToString:loginPassword]) {
                

                if ([self checkHandwriting]) {
                    
                    [self.loginInfoLabel setStringValue:@"User authentificated!"];
                } else {
                    [self.loginInfoLabel setStringValue:@"Wrong handwriting!"];
                }
                
                
            } else {
                
                [self.loginInfoLabel setStringValue:@"Wrong pass or login"];
            }
            
            
        }
    }
    
    NSLog(@"loginTimeIntervals CLEANED\n");
    [self.loginPasswordTextField setStringValue:@""];
    [self.loginTimeIntervals removeAllObjects];
}


- (BOOL)checkHandwriting
{
    int numberOfIntervals = (int)self.loginTimeIntervals.count - 1;
    float *intervals = malloc (sizeof (float) * numberOfIntervals);
    for (int index = 0; index < numberOfIntervals; index++) {
        NSDate *firstDate = self.loginTimeIntervals[index];
        NSDate *secondDate = self.loginTimeIntervals[index + 1];
        NSTimeInterval executionTime = [secondDate timeIntervalSinceDate:firstDate];
        intervals[index] = (float)executionTime;
    }
    
    
    //Mi Si^2 Si
    float *Mi = malloc (sizeof (float) * numberOfIntervals);
    float *S2i = malloc (sizeof (float) * numberOfIntervals);
    float *Si = malloc (sizeof (float) * numberOfIntervals);
    float *Tp = malloc (sizeof (float) * numberOfIntervals);
    for (int i = 0; i < numberOfIntervals; i++) {
        
        //у '= у ^ {yi}
        float *intervalY = malloc (sizeof (float) * numberOfIntervals - 1);
        for (int n = 0; n < numberOfIntervals - 1; n++) {
            if (n < i) {
                intervalY[n] = intervals[n];
            } else {
                intervalY[n] = intervals[n + 1];
            }
        }
        
        //calculate Mi
        float sumY = 0;
        for (int k = 0; k < numberOfIntervals - 1; k++) {
            sumY += intervalY[k];
        }
        Mi[i] = sumY/(numberOfIntervals - 1);
        
        //calculate S^2i
        sumY = 0;
        for (int k = 0; k < numberOfIntervals - 1; k++) {
            sumY += (intervalY[k] - Mi[i])*(intervalY[k] - Mi[i]);
        }
        S2i[i] = sumY/(numberOfIntervals - 1 - 1);
        
        Si[i] = sqrtf(S2i[i]);
        
        Tp[i] = (intervals[i] - Mi[i])/Si[i];
        if (Tp[i] < 0) Tp[i] = (-Tp[i]);
        

    }
    
    
    float *etalonInter = malloc (sizeof (float) * numberOfIntervals);
    float *etalonInterMi = malloc (sizeof (float) * numberOfIntervals);
    float *etalonS2i = malloc (sizeof (float) * numberOfIntervals);
    float *SSS = malloc (sizeof (float) * numberOfIntervals);
    float *etalonTp = malloc (sizeof (float) * numberOfIntervals);
    for (NSMutableDictionary *userInfo in self.plistArray) {
        if ([userInfo[@"login"] isEqualToString:[self.loginLoginTextField stringValue]]) {
            
            NSString *previousString = userInfo[@"Inter"];
            NSArray *components = [previousString componentsSeparatedByString:@"/"];
            for (int g = 0; g < components.count; g++) {
                etalonInter[g] = [components[g] floatValue];
            }
        }
    }
    
//    for (int o = 0; o < numberOfIntervals; o++) {
//        NSLog(@"etalon %f", etalonInter[o]);
//    }

    //etalon Mi
    
//    for (int i = 0; i < numberOfIntervals; i++) {
//        
//        //у '= у ^ {yi}
//        float *intervalY = malloc (sizeof (float) * numberOfIntervals - 1);
//        for (int n = 0; n < numberOfIntervals - 1; n++) {
//            if (n < i) {
//                intervalY[n] = etalonInter[n];
//            } else {
//                intervalY[n] = etalonInter[n + 1];
//            }
//        }
//        
//        //calculate уефдщтMi
//        float sumY = 0;
//        for (int k = 0; k < numberOfIntervals - 1; k++) {
//            sumY += intervalY[k];
//        }
//        etalonInterMi[i] = sumY/(numberOfIntervals - 1);
//        
//        //calculate S^2i
//        sumY = 0;
//        for (int k = 0; k < numberOfIntervals - 1; k++) {
//            sumY += (intervalY[k] - etalonInterMi[i])*(intervalY[k] - Mi[i]);
//        }
//        etalonS2i[i] = sumY/(numberOfIntervals - 1 - 1);
//        
//    }
    
    
//    for (int i = 0; i < numberOfIntervals; i++) {
//        
//        float a = (etalonS2i[i]*etalonS2i[i] + S2i[i]*S2i[i]) * (numberOfIntervals - 1);
//        a = a / (2 * numberOfIntervals - 1);
//        
//        SSS[i] = sqrtf(a);
//        
//    }
    
    
    
    
    //-----
    
    
    
    
    //-----
    for (int o = 0; o < numberOfIntervals; o++) {
        NSLog(@"Compare %f with %f\n", etalonInter[o], intervals[o]);
    }

    
    int result = 0;
    for (int f = 0; f < numberOfIntervals; f++) {
        
        if ([self compare:etalonInter[f] withInput:intervals[f]]) {
            result++;
        }
    }
    
    float pers = 0.8;
    NSLog(@"\nIn PEsrsents : %f \n", result/(pers*numberOfIntervals));
    if (result/(pers*numberOfIntervals) > pers) return YES;
    
    
    //---
//    float s2Max = 0;
//    float s2Min = 0;
//    
////#warning cool code!
//    float *fisherCoef = malloc (sizeof (float) * numberOfIntervals);
//    for (NSMutableDictionary *userInfo in self.plistArray) {
//        if ([userInfo[@"login"] isEqualToString:self.learnLogin]) {
//            
//            NSString *previousString = userInfo[@"Si"];
//            NSArray *components = [previousString componentsSeparatedByString:@"/"];
//            for (int g = 0; g < components.count; g++) {
//                float b = [components[g] floatValue];
//                
//                s2Max = MAX(Si[g]*Si[g], b*b);
//                s2Min = MIN(Si[g]*Si[g], b*b);
//                
//                fisherCoef[g] = s2Max/s2Min;
//            }
//        }
//    }
//    //--
    
    
    return NO;
}

- (BOOL)compare:(float)etalon withInput:(float)input
{
    float top = etalon * ((1 - self.persise) + 1);
    float buttom = etalon * self.persise;
    
    if (input < top && input > buttom) {
        return YES;
    }
    return NO;
}

@end






