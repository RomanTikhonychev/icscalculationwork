//
//  main.m
//  ICSCalculationWork
//
//  Created by Рома on 5/26/16.
//  Copyright © 2016 RomanTikhonychev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
